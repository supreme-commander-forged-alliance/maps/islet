## Islet

_A map for Supreme Commander: Forged Alliance (Forever)_

![](/images/map-view.png)

_After years of nearly indefinite production the previously glorious planet of Ikara has been almost entirely changed into an ocean. Due to global warming the sea levels have risen to almost unbearable levels, slowly but surely swallowing all of the land while rising. Nevertheless, the planet is wealthy of natural minerals and regardless of the climate's condition: war rages on._

## Story surrounding the map

![](/images/island-view.png)

The story of the map circles around the rising sea level. The sea level is rising due to the climate change that causes the entire earth to become warmer - melting ice caps in return. This is a simplified explanation, the entire process is complex and is generally tagged as the _greenhouse effect_ or as _global warming_.

As of right now, islands that exist in the oceans that have existed for thousands of years are dissapearing. These islands are only centimeters above sea level, therefore any rise of the sea level will be devastating. Examples are:
 - Tuvalu's sinking islands
 - Islands in Fiji
 - Micronesia

For a more visual representation of what is happening, please view:
 - https://www.nbcnews.com/mach/science/three-islands-disappeared-past-year-climate-change-blame-ncna1015316

The map is trying to picture the same issues. Some parts of the islands are already flooded, rendering the ground unbuildable. Other parts contain units of the past that still remain as reclaim on the bottom of the what we now call the ocean. 

## Statistics of the map

![](/images/center-view.png)

The map is designed to be either be played as a 2 vs 2 or a 3 vs 3. Both teams start on identical islands, with identical oppertunities. All players start with 8 or 9 mass extractors and 1 - 2 hydrocarbons available to them. An additional 16 mass extractors are spread over various small islands that are located towards the center of the map.

There is mass reclaim on the map due to the expensive nature of navy units. This includes:
 - Rocks on and surrounding the starting islands.
 - A few boats (t1 frigates / t2 destroyers) on the smaller islands.
 - A few ground units (t1 tanks / t2 tanks) spread out throughout the map.
 - A lot of ground units (t2 tanks / t3 assault bots) on the bottom of the ocean in the middle of the map.

The map is sufficient in size and dynamics (number of available paths to other players) to allow for diverse and interesting navy gameplay.

## Notes about the imagery

 - The yellow boundary represents the playable area. Therefore the map is roughly 25x15, instead of 40x20.
 - The units with orange icons represent the neutral civilians.
 - The units with nearly black icons represent the reclaim that is available on the map.

## Information about the map

Stratum 2 and 3 are made by rendering clouds in Adobe Photoshop and then subtracting the 'above water' mask. A simialar process is applied to the heightmap where a very faint rendering of clouds is added to create the ocean floor.

The heightmap scaling is set to 53 (white) to 0 (black) in the GPG editor.

## License

All assets are licensed with [CC-BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).