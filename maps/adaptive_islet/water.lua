Water = {
surface_elev = 25.3,
deep_elev = 0,
abyss_elev = 0,
surface_color = { 0.14, 0.9, 0.66 },
color_lerp = { 0, 0.2 },
refraction_scale = 0.1375,
fresnel_bias = 0,
fresnel_power = 2,
unit_reflection = 0.9,
sky_reflection = 0.788,
sun_shininess = 63.3,
sun_strength = 9.4,
sun_direction = { 0.0995482, -0.962631, 0.251857 },
sun_color = { 0.35, 0.19, 0.09 },
sun_reflection = 4.85,
sun_glow = 0.1,
local_cubemap = '/textures/environment/SkyCube_TropicalOp06a.dds',
water_ramp = '/textures/engine/waterramp.dds',
normal_repeat = { 0.0009, 0.009, 0.04, 0.7 },
normals = {
['0'] = {
normal_movement = { -0.564836, 0.0995959 },
local_normals = '/textures/engine/waves.dds',
},
['1'] = {
normal_movement = { -0.0822387, 0.0690065 },
local_normals = '/textures/engine/waves000.dds',
},
['2'] = {
normal_movement = { -0.0311424, -0.00549124 },
local_normals = '/textures/engine/waves001.dds',
},
['3'] = {
normal_movement = { 0.000788689, 0.000661788 },
local_normals = '/textures/engine/waves001.dds',
},
},
}
