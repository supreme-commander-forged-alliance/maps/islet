version = 3
ScenarioInfo = {
    name = 'Adaptive Islet',
    description = "After years of nearly indefinite production the previously glorious planet of Ikara has been almost entirely changed into an ocean. Due to global warming the sea levels have risen to almost unbearable levels, slowly but surely swalling all of the land while rising. Nevertheless, the planet is wealthy of natural minerals and regardless of the climate's condition: war rages on. The playable area of the map is roughly 25x15. Map made by (Jip) Willem Wijnia. You can find the latest version at: https://gitlab.com/supreme-commander-forged-alliance",
    type = 'skirmish',
    starts = true,
    preview = '',
    size = {2048, 1024},
    map_version = 4,
    map = '/maps/adaptive_islet/adaptive_islet.scmap',
    save = '/maps/adaptive_islet/adaptive_islet_save.lua',
    script = '/maps/adaptive_islet/adaptive_islet_script.lua',
    norushradius = 0.000000,
    norushoffsetX_ARMY_1 = 0.000000,
    norushoffsetY_ARMY_1 = 0.000000,
    norushoffsetX_ARMY_2 = 0.000000,
    norushoffsetY_ARMY_2 = 0.000000,
    norushoffsetX_ARMY_3 = 0.000000,
    norushoffsetY_ARMY_3 = 0.000000,
    norushoffsetX_ARMY_4 = 0.000000,
    norushoffsetY_ARMY_4 = 0.000000,
    norushoffsetX_ARMY_5 = 0.000000,
    norushoffsetY_ARMY_5 = 0.000000,
    norushoffsetX_ARMY_6 = 0.000000,
    norushoffsetY_ARMY_6 = 0.000000,
    Configurations = {
        ['standard'] = {
            teams = {
                { name = 'FFA', armies = {'ARMY_1','ARMY_2','ARMY_3','ARMY_4','ARMY_5','ARMY_6',} },
            },
            customprops = {
                ['ExtraArmies'] = STRING( 'RECLAIM NEUTRAL_CIVILIAN ARMY_17' ),
            },
        },
    }}
